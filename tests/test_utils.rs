extern crate dedup;

mod tests {
    use std::path::Path;
    use dedup::*;

    #[test]
    fn test_recursive() {
        let mut path = Path::new("./").canonicalize().unwrap();
        path.push("dir_example");
        let list = utils::recursive_read_dir(&path).unwrap();
        assert_eq!(list.len(), 4);
    }
}