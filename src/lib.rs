pub mod utils {
    use std::path;
    use std::io::Error;

    pub fn recursive_read_dir (path: &path::Path) -> Result<Vec<path::PathBuf>, Error> {
        let mut list : Vec<path::PathBuf> = Vec::new();
        if path.is_dir() {
            for entry in path.read_dir()? {
                if let Ok(entry) = entry {
                    let tmp = entry.path();
                    if tmp.is_dir() {
                        let mut deeper = super::utils::recursive_read_dir(tmp.as_path())?;
                        list.append(&mut deeper);
                    } else {
                        list.push(tmp)
                    }
                }
            }
        }
        return Ok(list);
    }
}
