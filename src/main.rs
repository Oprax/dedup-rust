extern crate sha3;
extern crate serde_json;
extern crate dunce;

extern crate dedup;

use std::path::Path;
use std::fs;
use std::collections::HashMap;

use sha3::{Digest, Sha3_256};
use dedup::utils;

fn main() {
    let root = Path::new("./").canonicalize()
        .expect("Path can't canonicalize");
    let root = dunce::canonicalize(&root).expect("Path can't canonicalize");
    let mut path = root.clone();
    path.push("dir_example");
    let mut files = HashMap::new();
    if path.is_dir() {
        let list = utils::recursive_read_dir(&path).expect("Can't read directory");
        for f in list {
            let fpath = f.to_str().unwrap().to_string();
            println!("F is : {}", fpath);
            let mut hasher = Sha3_256::default();
            let buffer = fs::read(f).expect("Can't read file");
            hasher.input(&buffer[..]);
            let hresult = format!("{:x}", hasher.result());
            println!("F SHA3-256 : {}", hresult);
            files.entry(hresult).or_insert(Vec::new()).push(fpath);
        }
    }
    println!("{:?}", files);
    let files_json = serde_json::to_string_pretty(&files).expect("JSON serialize failed");
    println!("json: '{}'", files_json);
    let mut jsonf = root.clone();
    jsonf.push(".cache.json");
    fs::write(jsonf, files_json).expect("Can't write JSON file");
}
